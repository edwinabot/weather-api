import os

import requests

from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

API_KEY = os.getenv('OWM_KEY', '')

# Create your views here.
class CurrentWeather(APIView):
    permission_classes = [permissions.AllowAny]

    def check_current_weather(self, city):
        weather = requests.get('http://api.openweathermap.org/data/2.5/weather?q=%s&APPID=%s' % (city, API_KEY))
        parsed = weather.json()
        description = parsed['weather'][0]['description']
        temp = parsed['main']['temp']
        hum = parsed['main']['humidity']
        fulfillment = {'fulfillmentText': "Current condition is %s, temperature is %.1f° (Celcius) and %s percent humidity." % (description, temp -272.15, hum)}
        return fulfillment

    def post(self, request):
        parameters = request.data['queryResult']['parameters']
        action = request.data['queryResult']['action']
        if action == 'current_conditions':
            fulfillment = self.check_current_weather(parameters['city'])
        return Response(fulfillment)
